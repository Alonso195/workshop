<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Variables de Productos.
     *
     * @var array
     */
    
    protected $fillable = ['sku','name','description','stock', 'id_category','price'];   
}
