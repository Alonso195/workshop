<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Aquí se agregan los nuevos campos.
     *
     * @var array
     */
    protected $fillable = ['name','description'];
}
