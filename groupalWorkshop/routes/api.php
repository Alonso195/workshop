<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth.basic'])->group(function () {
    Route::get('category', 'CategoryController@index');
    Route::get('category/{id}', 'CategoryController@show');

    Route::post('category', 'CategoryController@store')->name('post');

    Route::PUT('category/{id}', 'CategoryController@update',function ($request,$id){ 
    return 'productos'.$id;
    });
    
    Route::delete('category/{id}', 'CategoryController@destroy');
});

Route::middleware(['auth.basic'])->group(function () {
    Route::get('product', 'ProductController@index');
    Route::get('product/{id}', 'ProductController@show');

    Route::post('product', 'ProductController@store')->name('post');

    Route::PUT('product/{id}', 'ProductController@update',function ($request,$id){ 
    return 'productos'.$id;
    });
    
    Route::delete('product/{id}', 'ProductController@destroy');
});