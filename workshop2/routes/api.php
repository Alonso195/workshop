<?php

use Illuminate\Http\Request;
use App\Http\Middleware\basicAuth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware(['jwt.auth'])->group(function () {
    Route::get('student', 'StudentController@index');

    Route::post('student', 'StudentController@store')->name('post');

    Route::PUT('student/{id}', 'StudentController@update',function ($request,$id){ 
    return 'productos'.$id;
    });

    Route::delete('student/{id}', 'StudentController@destroy');
});

Route::post('login', 'AuthenticateController@authenticate')->name('login');

// Rutas con basic auth
/*
Route::middleware(['auth.basic'])->group(function () {
    Route::get('student', 'StudentController@index');

    Route::post('student', 'StudentController@store')->name('post');

    Route::PUT('student/{id}', 'StudentController@update',function ($request,$id){ 
    return 'productos'.$id;
    });

    Route::delete('student/{id}', 'StudentController@destroy');
});

*/

//Routas Normales 
/*
Route::get('student', 'StudentController@index');

Route::post('student', 'StudentController@store')->name('post');

Route::PUT('student/{id}', 'StudentController@update',function ($request,$id){ 
    return 'productos'.$id;
  });

  Route::delete('student/{id}', 'StudentController@destroy');
*/