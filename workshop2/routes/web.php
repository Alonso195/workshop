<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Routas Normales 
/*
Route::get('student', 'StudentController@index');

Route::post('student', 'StudentController@store')->name('post');

Route::PUT('student/{id}', 'StudentController@update',function ($request,$id){ 
    return 'productos'.$id;
  });

  Route::delete('student/{id}', 'StudentController@destroy');
*/