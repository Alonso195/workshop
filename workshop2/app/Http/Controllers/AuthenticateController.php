<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
       // return $credentials;

        try{
            $token = JWTAuth::attempt($credentials);
            if(!$token){
                return response()->json(['error' => 'invalid_credentials'], 401);

            }
        } catch(JWTExeption $e){
            return response()->json(['error' => 'could_not_create_session'], 500);
        }

        $response = compact('token');
        $response['user'] = Auth::user();
        return $response;
    }
}
