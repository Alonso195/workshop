<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index(){
       $students =  Student::all();
       return response()->json($students, 200);
    }

    public function store(Request $request){
        $students = Student::create($request->all());
        return response()->json($students, 201);
    }

    public function update(Request $request, $id){
        $student = Student::find($id);

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $email = $request->input('email');
        $address = $request->input('address');

        $student->firstname = $firstname;
        $student->lastname = $lastname;
        $student->email = $email;
        $student->address = $address;

        $student->save();
        return response()->json($student, 202);
    }

    public function destroy($id){
        $student = Student::find($id);

        if($student){
            $student->delete();
            return response()->json($student, 204);  
        } else{
            return response()->json($student,404);
        }
    }
}
